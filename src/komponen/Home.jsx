import React, { Component } from 'react';
import Navbar from './Navbar';
import './Home.css';
import Item from './Item';
import { Link } from 'react-router-dom';

export default class Home extends Component {
    constructor() {
        super();
        this.state = {
            length: 0,
            products: [],
        };
    }

    handleBuy() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    componentDidMount() {
        fetch('https://fakestoreapi.com/products?limit=9')
            .then((res) => res.json())
            .then((res) =>
                this.setState({
                    length: res.length,
                    products: res,
                })
            );
    }

    render() {
        const { products } = this.state;
        const productsItem = products.map((product) => {
            return product;
        });

        return (
            <div id="Home">
                <Navbar />
                <div className="container-fluid mt-5 text-white">
                    <div className="row">
                        <div className="col-lg-2 ml-5">
                            <Link className="btn btn-success" to="/Pembayaran">
                                Lakukan Pembayaran
                            </Link>
                        </div>
                    </div>

                    <div className="row">
                        <div className="d-sm-flex justify-content-around flex-wrap">
                            {productsItem.map((productItem) => {
                                return (
                                    <Item
                                        name={productItem.title}
                                        image={productItem.image}
                                        price={productItem.price}
                                        id={productItem.id}
                                        buy={() => {
                                            this.handleBuy();
                                        }}
                                    />
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
