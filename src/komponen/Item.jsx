import React from 'react';

class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
        };
    }

    increment() {
        this.setState({ count: this.state.count + 1 });
    }

    decrement() {
        this.setState({ count: this.state.count - 1 });
    }

    render() {
        return (
            <div key={this.props.id} className="container-1 m-5 bg-secondary">
                <img
                    src={this.props.image}
                    className="card-img-top mt-3"
                    alt={this.props.id}
                    onClick={() => {
                        this.handleClickItem();
                    }}
                />
                <div className="card-body text-center">
                    <p className="card-text">{this.props.name}</p>
                    <p className="card-text">$ {this.props.price}</p>
                    <button
                        onClick={this.props.buy}
                        className="btn btn-danger mb-3"
                    >
                        Buy
                    </button>
                    <br />
                    <button
                        className="btn btn-info mx-3"
                        onClick={() => {
                            this.decrement();
                        }}
                    >
                        -
                    </button>
                    <span>{this.state.count}</span>
                    <button
                        className="btn btn-info mx-3"
                        onClick={() => {
                            this.increment();
                        }}
                    >
                        +
                    </button>
                </div>
            </div>
        );
    }
}

export default Item;
