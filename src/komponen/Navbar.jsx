import React from 'react';
import '../asset/bootstrap.css';
import { Link } from 'react-router-dom';

class Navbar extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <Link
                    style={{
                        fontFamily: 'Cedarville Cursive',
                        fontSize: '22px',
                    }}
                    className="navbar-brand"
                    to="/"
                >
                    Tio Shop
                </Link>

                <div className="navigation-shop">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link className="nav-link" to="/Home">
                                Home
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/Pembayaran">
                                Payment
                            </Link>
                        </li>

                        <li className="nav-item">
                            <Link className="nav-link" to="/About">
                                About
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navbar;
