import React from 'react';
import Navbar from '../komponen/Navbar';

export default function About() {
    return (
        <div className="bg-secondary">
            <Navbar />
            <div className="jumbotron bg-transparent text-center">
                <h1 className="text-white">Profile</h1>
                <img
                    src="https://pbs.twimg.com/profile_images/421675195389075457/ZoX-wzkM_400x400.jpeg"
                    class="rounded-circle fp"
                    alt="Foto Profil"
                    width="250"
                    height="250"
                />
            </div>

            <div className="container-fluid text-center text-white">
                <div className="row">
                    <div className="col-md-12">
                        <h2
                            style={{
                                fontFamily: 'Cedarville Cursive',
                                fontSize: '2em',
                            }}
                        >
                            Sulistio
                        </h2>
                        <br />
                        <h4>Address: Jl Marzuki VII RT 09 RW01, Jakarta</h4>
                        <h5>E-mail: race2112@aol.com</h5>
                        <h5>Phone: 085770429988</h5>
                    </div>
                </div>
            </div>
        </div>
    );
}
