import React, { useState } from 'react';
import Navbar from '../komponen/Navbar';

function Pembayaran() {
    const [isPay, setIsPay] = useState(false);

    let modal;
    if (isPay) {
        modal = (
            <div className="jumbotron bg-transparent text-center">
                <h2 className="text-info">Selamat Pembayaran berhasil</h2>
                <h2 className="text-info">Silahkan tunggu pesanan anda ya</h2>
            </div>
        );
    }

    return (
        <div id="payment" className="text-center bg-secondary">
            <Navbar />
            <h3 className="mt-5">Total Belanja Anda $ xxx.xxx</h3>
            <button
                onClick={() => {
                    setIsPay(true);
                }}
                className="btn btn-success mt-2 mb-5"
            >
                Klik untuk membayar
            </button>
            {modal}
        </div>
    );
}

export default Pembayaran;
