import React from 'react';
import './App.css';
import Home from './komponen/Home';
import { Route } from 'react-router-dom';
import Login from './komponen/Login';
import Pembayaran from './fungsi_komponen/Pembayaran.jsx';
import About from './fungsi_komponen/About';

function App() {
    return (
        <div className="App">
            <Route exact path="/" component={Login} />
            <Route path="/Home" component={Home} />
            <Route path="/Pembayaran" component={Pembayaran} />
            <Route path="/About" component={About} />
        </div>
    );
}

export default App;
